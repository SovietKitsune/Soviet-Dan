return {
    name = 'User',
    schema = {
        id = {type = 'number', identifier = true},
        money = {type = 'number', default = 0},
        inventory = {type = 'table', default = {}},
        equipped = {type = 'table', default = {}},
        skill = {type = 'number', default = 1},
        xp = {type = 'number', default = 0},
        debt = {type = 'number', default = 0}
    }
}