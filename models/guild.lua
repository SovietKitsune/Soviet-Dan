return {
    name = 'Guild',
    schema = {
        id = {type = 'number', identifier = true},
        money = {type = 'number', default = 0},
        debt = {type = 'number', default = 0},
        prefix = {type = 'string', default = client.prefix},
        restrictedChannels = {type = 'table', default = {}},
        roles = {type = 'table', default = {}},
        shop = {type = 'table', default = {}},
        welcomer = {type = 'number', default = 0},
        leaver = {type = 'number', default = 0},
        joinRole = {type = 'number', default = 0},
        joinableRoles = {type = 'table', default = {}}
    }
}
