local fun = Class('Fun', client.cog)

local RichEmbed = client:import('embed')
local games = {}

function fun:initialize(Client, schemas)
    client.cog.initialize(self, Client)

    local promise = client:import('promise')
    local timer = client:require('timer')

    self.russianroulette = self.command:new(function(msg,args)
        if games[msg.guild.id] then
            msg.channel:send('A game is already in progress, please wait till it ends')
            return
        end

        games[msg.guild.id] = true

        local embed = RichEmbed:new()
        :setTitle('Russian Roulette')
        :setDescription('<@'..msg.author.id ..'> started a "friendy" game of Russian Roulette. React below to join in the fun. Starting in 10 seconds!')
        :setAuthor(msg.author.name, msg.author.avatarURL)

        local m = msg.channel:send(embed:getTable())

        m:addReaction('✅')

        local players = {}

        function update(msg, content)
            local co = coroutine.create(function(msg, content)
                msg:setEmbed(content:getRaw())
                coroutine.yield()
            end)

            coroutine.resume(co,msg,content)
        end

        timer.setTimeout(10000, function()
            local reactions = m.reactions
            reactions:forEach(function(n)  --[[]]
                if n.emojiName == '✅' then
                    local handle = coroutine.create(function(reactions, client, embed, m, players, msg)
                        local users = reactions:getUsers()
                        local creator = false
                        users:forEach(function(u)
                            if not(u.id == client.dClient.user.id) then
                                table.insert(players, u)
                                if u.id == msg.author.id then
                                    creator = true
                                end
                            end
                        end)

                        if creator == false then
                            table.insert(players, msg.author)
                        end

                        if(#players <= 1) then
                            m.channel:send('Not enough people joined')
                            games[msg.guild.id] = false
                        else
                            local newEmbed = RichEmbed:new()
                            :setTitle('Russian Roulette')
                            :setDescription('Ok, we go for until somebody die- stops intaking oxygen')
                            :setAuthor(msg.author.name, msg.author.avatarURL)
                            local message = m.channel:send(newEmbed:getTable())
                            local nobodyOut = true
                            local out

                            local count = 1

                            local function go()
                                count = count + 1
                                if count > #players then
                                    count = 1
                                end
                                if not nobodyOut then
                                    newEmbed:setDescription(out.name .. ' died, everybody else lives')

                                    update(message, newEmbed)
                                end
                                timer.setTimeout(2500, function()
                                    local first = players[count]
                                    newEmbed:setDescription(first.name .. ' will go')
                                    update(message, newEmbed)
                                    timer.setTimeout(1000, function()
                                        local lose = math.random(1,6)
                                        if lose == 3 then
                                            nobodyOut = false
                                            out = first
                                            newEmbed:setDescription(first.name .. ' died, everyone else survived')
                                            update(message, newEmbed)
                                        else
                                            newEmbed:setDescription(first.name .. ' survived')
                                            update(message, newEmbed)
                                            go()
                                        end
                                    end)
                                end)
                            end

                            go()
                        end
                        games[msg.guild.id] = false
                        coroutine.yield()
                    end)

                    coroutine.resume(handle, n, client, embed, m, players, msg)
                end
            end)
        end)
    end,{
        description = 'Play a nice game of Russian Roulette',
        aliases = {'rr'}
    })
end

client:addCog(fun:new(client))
