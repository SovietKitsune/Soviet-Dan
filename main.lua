local discord = require('./helpers')
local fs = require('fs')
-- If you want to load .env you need to have you token in it,
-- whats the point of .env if you not gonna use it for your token
local client = discord.client:new(';','env;TOKEN', {
    owner = '525840152103223338',
    loadDefaultCogs = true,
    firebase = './models' -- Enables firebase module and settings
})

local cogs = fs.readdirSync('./cogs')

for _,v in pairs(cogs) do
    if string.endswith(v,'.lua') then
        local name = v:sub(1 , #v - 4)

        require('./cogs/' .. name)
    end
end

client:run({
    name = 'Cogs are fun',
    type = 1
})
