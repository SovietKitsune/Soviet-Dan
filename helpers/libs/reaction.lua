--[=[
@c Reaction
@t ui
@p channel channel
@op settings dictionary
@d A reaction table
]=]

local middleClass = require('../util/class')
local timer = require('timer')
local promise = require('./promise')
local reaction = middleClass('Reaction Buttons')

local client = _G.client

local calls = {}

log = function(...)
    local data = {...}
    for _,v in pairs(data) do
        print(string.format('%s | \27[1;33m[DEBUG]\27[0m   | %s', os.date('%F %T'), v))
    end
end

function reaction:initialize(channel, settings)
    self.type = settings.type or 'menu'

    if not settings.reactions then
        error('No reactions/messages set')
    end
    
    self.reactions = settings.reactions
    self.messages = settings.messages

    if not settings.messages then
        self.messages = {settings.message}
    end

    self.timeout = settings.timeout or 30000
    self.callName = tostring(math.random(1,100000000))

    local pos = 1
    local m

    local Promise = promise.new()
    if settings.embeds then
        local co = coroutine.create(function(m)
            Promise:resolve(channel:send(self.messages[1]:getTable()))
        end)

        coroutine.resume(co,m, Promise)
        
    else
        local co = coroutine.create(function(m)
            Promise:resolve(channel:send(self.messages[1]))
        end)

        coroutine.resume(co,m, Promise)
    end
    
    Promise:next(function(m) 
        local co = coroutine.create(function(m,timer, calls,self, settings)

            for _,v in pairs(self.reactions) do
                m:addReaction(v.emoji)
            end
        
            local actions = {}

            function actions.endReaction()
                calls[self.callName] = nil
            end

            if self.type == 'menu' then
                function actions.forward()
                    local changed = true
                    pos = pos + 1
                    if pos > #self.messages then
                        changed = false
                        pos = #self.messages 
                    end
            
                    if changed then
                        if settings.embeds then
                            --local co = coroutine.create(function(embed)
                            m:setEmbed(self.messages[pos]:getRaw())
                                --coroutine.yield()
                            --end)
                    
                            --coroutine.resume(co,self.messages[pos])
                        else
                            --local co = coroutine.create(function(msg)
                                m:setContent(self.messages[pos])
                                --coroutine.yield()
                            --end)
                    
                            --coroutine.resume(co,self.messages[pos])
                        end
                    end
                end
            
                function actions.back()
                    local changed = true
                    pos = pos - 1
                    if pos <= 0 then
                        changed = false
                        pos = 1
                    end
            
                    if changed then
                        if settings.embeds then
                            --local co = coroutine.create(function(embed)
                                m:setEmbed(self.messages[pos]:getRaw())
                                --coroutine.yield()
                            --end)
                    
                            --coroutine.resume(co,self.messages[pos])
                        else
                            --local co = coroutine.create(function(msg)
                                m:setContent(self.messages[pos])
                                --coroutine.yield()
                            --end)
                    
                            --coroutine.resume(co,self.messages[pos])
                        end
                    end
                end
            end
        
            calls[self.callName] = function(reaction,user)
                if reaction.message.id == m.id and user ~= m.author.id then 
                    -- Nothing can go wrong
                    for _,v in pairs(self.reactions) do
                        if v.emoji == reaction.emojiHash then
                            v.action(m, actions)
                            reaction.message:removeReaction(reaction.emojiHash, user)
                        end
                    end
                end
            end
        
            timer.setTimeout(self.timeout, function() 
                calls[self.callName] = nil
            end)
        end)

        coroutine.resume(co, m,timer, calls,self, settings)
    end,error)
end

return function()
    client:on('reactionAdd', function(reaction,user) 
        for _,v in pairs(calls) do
            if v then
                v(reaction,user)
            end
        end
    end)

    return reaction
end