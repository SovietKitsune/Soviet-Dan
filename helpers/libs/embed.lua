--[=[
@c Embed
@t ui
@d A constructor for rich embeds for messages
]=]

local middleClass = require('../util/class')
local embed = middleClass('Embed')

embed.colors = {
    lightRed = 0xff7675,
    red = 0xd63031,
    lightBlue = 0x74b9ff,
    blue = 0x0984e3,
    lightGreen = 0x55efc4,
    green = 0x00b894,
    lightPurple = 0xa29bfe,
    purple = 0x6c5ce7,
    lightYellow = 0xffeaa7,
    yellow = 0xfdcb6e,
    lightOrange = 0xfab1a0,
    orange = 0xe17055,
    lightGray = 0xdfe6e9,
    gray = 0xb2bec3,
    darkGrey = 0x636e72,
    lightBlack = 0x2d3436,
    lightPink = 0xfd79a8,
    pink = 0xe84393,
    lightTeal = 0x81ecec,
    teal = 0x00cec9
}

function embed:initialize()
    self.embed = {}
    self.noIndex = {}

    for _,v in pairs(self.colors) do
        table.insert(self.noIndex, v)
    end
end

--[=[
@m setTitle
@p title string
@r Embed
@d Sets the title of the Rich embed
]=]
function embed:setTitle(title)
    self.embed.title = title
    return self
end

--[=[
@m setDescription
@p desc string
@r Embed
@d Sets the description of the Rich embed
]=]
function embed:setDescription(desc)
    self.embed.description = desc
    return self
end

--[=[
@m setColor
@p color hex
@r Embed
@d Sets the color of the Rich embed
]=]
function embed:setColor(color)
    self.embed.color = color
    return self 
end

--[=[
@m addField
@p name string
@p value string
@op inline boolean
@r Embed
@d Adds a field to the embed
]=]
function embed:addField(name,value, inline)
    if not inline then inline = false end

    if not self.embed.fields then self.embed.fields = {} end
    
    table.insert(self.embed.fields, {name = name, value = value, inline = inline})

    return self 
end

--[=[
@m setAuthor
@p name string
@op icon string 
@op url string
@r Embed
@d Sets the author of the Rich embed
]=]
function embed:setAuthor(name, icon, url)
    if not icon then icon = "" end if not url then url = "" end

    self.embed.author = {
        name = name,
        icon_url = icon,
        url = url
    }

    return self 
end

--[=[
@m setFooter
@p text string
@op icon string 
@r Embed
@d Sets the footer of the Rich embed
]=]
function embed:setFooter(text, icon)
    if not icon then icon = "" end

    self.embed.footer = {
        text = text,
        icon_url = icon
    }

    return self 
end

--[=[
@m setImage
@p image string
@r Embed
@d Sets the image of the Rich embed
]=]
function embed:setImage(img) 
    self.embed.image = {
        url = img
    }

    return self 
end

--[=[
@m setThumbnail
@p url string
@r Embed
@d Sets the thumbnail of the Rich embed
]=]
function embed:setThumbnail(url)
    self.embed.thumbnail = {
        url = url
    }

    return self 
end

--[=[
@m setThumbnail
@op date string
@r Embed
@d Sets the timestamp of the Rich embed. Must be in ISO 8601 format
]=]
function embed:setTimestamp(date)
    if not date then date = os.date("!%Y-%m-%dT%TZ") end
    self.embed.timestamp = date 

    return self 
end

--[=[
@m setUrl
@p url string
@r Embed
@d Sets the url of the Rich embed
]=]
function embed:setURL(url)
    self.embed.url = url

    return self 
end

--[=[
@m getTable
@r table
@d Returns the table to be sent in a message
]=]
function embed:getTable()
    if not self.embed['color'] then
        local color = self.noIndex[math.random(1,#self.noIndex)]
        self:setColor(color)
    end
    return {embed = self.embed}
end

--[=[
@m getRaw
@r Embed
@d Returns the embed to be used to edit an embed
]=]
function embed:getRaw()
    if not self.embed['color'] then
        local color = self.noIndex[math.random(1,#self.noIndex)]
        self:setColor(color)
    end
    return self.embed 
end

return embed