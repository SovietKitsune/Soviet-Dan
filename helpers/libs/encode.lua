--[=[
@c Coder
@t ui
@d A url encoder/decoder
]=]

local char_to_hex = function(c)
    return string.format("%%%02X", string.byte(c))
end

local hex_to_char = function(x)
    return string.char(tonumber(x, 16))
end
  
--[=[
@m encode
@p url string
@r string
@d Encode the url/string to be used in a url
]=]

local function urlencode(url)
    if url == nil then
      return
    end
    url = url:gsub("\n", "\r\n")
    url = url:gsub("([^%w ])", char_to_hex)
    url = url:gsub(" ", "+")
    return url
end

--[=[
@m decode
@p url string
@r string
@d Decode the url/string to be used
]=]
local function urldecode(url)
    if url == nil then
      return
    end
    url = url:gsub("+", " ")
    url = url:gsub("%%(%x%x)", hex_to_char)
    return url
end

return {
    decode = urldecode,
    encode = urlencode
}