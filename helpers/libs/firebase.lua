--[=[
@c Firebase
@t ui
@p root string
@p token string
@p error boolean
@p credentials table
@d A Firebase api manager
]=]

local json = require("json")
local http = require("coro-http")
local Promise = require('./promise')
local timer = require('timer')
local google = require('./google')

local middleclass = require('../util/class')

local firebase = middleclass('Firebase')
local Schema = middleclass('Firebase.Schema')
local Model = middleclass('Firebase.Model')

local refresh_coro = { }

local function reauthenticate(db)
    refresh_coro[db.root] = refresh_coro[db.root] or coroutine.wrap(function()
        local success, body = google.refresh(db.refresh, db.key)

        if not success then
            return print(string.format('%s | \27[1;33m[WARNING]\27[0m | %s', os.date('%F %T'), 'Request for Token Reauthentication Failed'))
        end

        db.refresh = body.refresh_token
        db.auth = body.id_token
        expires = os.time() + body.expires_in
    end)

    refresh_coro[db.root]()
end

Promise.async = function(callback)
    timer.setInterval(0, callback)
end

function firebase:initialize(root, auth, authtab, error,cb)
    self.root = root
    self.error = error

    _G.db = self

    coroutine.wrap(function()
        local success, body = google.email(authtab[1] or authtab.email, authtab[2] or authtab.password, auth)

        if not success then
            return print(string.format('%s | \27[1;33m[WARNING]\27[0m | %s', os.date('%F %T'), 'Request for Token Authentication Failed'))
        end

        self.email = body.email
        self.auth = body.idToken
        self.refresh = body.refreshToken
        self.expires = os.time() + body.expiresIn
        if process.argv[2] ~= 'genDocs' then
            print(string.format('%s | \27[1;32m[INFO]   \27[0m | %s', os.date('%F %T'), 'Firebase Authenticated: ' .. body.email))
            if cb then
                cb(self)
            end
        end
    end)()
end

function firebase:request(node,method,callback,content)
    local root = self.root

    root = root:gsub("https://","")
    root = root:gsub("http://","")
    root = root:gsub("www.","")
    
    if not self.key and not self.auth then return end

    if self.auth and self.expires <= os.time() - 30 then
        reauthenticate(self)
    end

    local url = "https://"..root..".firebaseio.com/"..node..".json?auth="..self.auth

    coroutine.wrap(function()
        local headers,body = http.request(method,url,{{"Content-Type","application/json"}},content)
        if callback then
            body = json.decode(body)

            callback(headers.code ~= 200 and body,body)
        end
    end)()
end

--[=[
@m newSchema
@p name string
@p schema dictionary
@r Schema
@d Create a new schema to create new models
]=]
function firebase:newSchema(name, schema)
    return Schema:new(name,schema, self, self.error)
end

--[=[
@m readModels
@p path string
@r dictionary
@d Create schemas for models in a dirrectory
]=]
function firebase:readModels(dir)
    local files = fs.readdirSync(dir)

    local models = {}
    for _,v in pairs(files) do
        if string.endswith(v,'.lua') then
            local name = v:sub(1 , #v - 4)

            local model = require(dir .. '/' .. name)
            table.insert(models,model)
        end
    end

    return models
end

--[=[
@c Schema
@t ui
@mt mem
@p name string
@p schema dictionary
@p request function
@d A Firebase schema
]=]

function Schema:initialize(name,schema, Self, Error)
    self.name = name
    self.error = Error
    self.schema = schema
    function self:request(node,method,callback,content)
        return Self:request(node,method,callback,content)
    end

    local id

    for i,v in pairs(schema) do
        if v.identifier then
            id = i
            if v.default then
                v.default = nil
            end
            break
        end
    end

    if not id then
        error('No identifier set')
    end

    self.id = id
end

--[=[
@m create
@p model dictionary
@r promise<Model>
@d Create a new model to store data
]=]

function Schema:create(model)
    local promise = Promise.new()

    for i,v in pairs(self.schema) do
        if not model[i] then
            if not self.schema[i].default then
                promise:reject('Value without default missing; ' .. i)
            else
                model[i] = self.schema[i].default 
            end
        else
            if not (type(model[i]) == self.schema[i] or type(model[i]) == self.schema[i].type) then
                promise:reject('Model type is not the same as schema type;')
            end
        end
    end

    local prevModel = model

    model = type(model) == "table" and json.encode(model) or model

    self:request(self.name .. '/' .. prevModel[self.id],"PUT",function(err,res) 
        if err then
            if self.error then
                print(err.error)
            end
            promise:reject(err)
        else
            local model = Model:new(self.schema, prevModel, self.name, prevModel[self.id], function (node,method,callback,content)
                return self:request(node,method,callback,content)
            end, self.error)
            for i,v in pairs(prevModel) do
                if type(v) == 'table' then
                    self:request(self.name .. '/' .. prevModel[self.id] .. '/' .. i,"PUT", function(err) 
                        if self.error then
                            print(err.error)
                        end
                    end, json.encode(v))
                end
            end
            promise:resolve(model)
        end
    end,model)

    return promise
end

--[=[
@m get
@p id any
@r promise<Model>
@d Get a model to store data
]=]
function Schema:get(id) 
    local promise = Promise.new()
    self:request(self.name .. '/' .. id,"GET",function(err,res) 
        if err then 
            if self.error then
                print(err.error)
            end
            promise:reject(err)
        else
            if not res then promise:resolve('None') end
            
            local model = Model:new(self.schema, res, self.name, id, function (node,method,callback,content)
                return self:request(node,method,callback,content)
            end)

            promise:resolve(model)
        end
    end)

    return promise
end

--[=[
@c Model
@t ui
@mt mem
@p schema dictionary
@p model dictionary
@p name string
@p id any
@p request function
@d A Firebase Model
]=]
function Model:initialize(schema, model, name, id, request,Error)
    self.schema = schema
    self.model = model
    self.name = name
    self.id = id
    self.cache = 'None'
    self.error = Error
    function self:request(node,method,callback,content)
        return request(node,method,callback,content)
    end
end

--[=[
@m update
@p change dictionary
@r promise<completion>
@d Update a model
]=]
function Model:update(change)
    local promise = Promise.new()
    local content = type(change) == "table" and json.encode(change) or change

    self:request(self.name .. '/' .. self.id,"PATCH",function(err,res) 
        if err then
            promise:reject(err)
        else
            for i,v in pairs(change) do
                if type(v) == 'table' then
                    local jsonData = json.encode(v)
                    self:request(self.name .. '/' .. self.id .. '/' .. i,"PATCH",function(err,res) 
                        if err then
                            if jsonData ~= '[]' and self.error then
                                print(err.error, i)
                            end
                        end
                    end, jsonData)
                end
                self.cache[i] = v
            end
            promise:resolve('Updated')
        end
    end,content)

    return promise
end

--[=[
@m getData
@r promise<Dictionary>
@d Get the models data
]=]
function Model:getData(cb)
    local promise
    if not cb then
        promise = Promise.new()
    end

    if self.cache ~= 'None' then
        local tb = {
            data = self.cache, 
            model = self
        }

        local mt = {}

        mt.__index = tb
        mt.__call = function(self)
            return self.model:update(self.data)
        end

        setmetatable(tb, mt)

        if cb then
            cb(tb)
        else
            promise:resolve(tb)
        end
    else
        self:request(self.name .. '/' .. self.id,"GET",function(err,res) 
            if err then 
                if self.error then
                    print(err.error)
                end
                promise:reject(err)
            else
                if not res then res = 'None' end
                self.cache = res

                local tb = {
                    data = res, 
                    model = self
                }

                local mt = {}

                mt.__index = tb
                mt.__call = function(self)
                    return self.model:update(self.data)
                end

                setmetatable(tb, mt)

                if cb then
                    cb(tb)
                else
                    promise:resolve(tb)
                end
            end
        end)
    end
    if not cb then
        return promise
    end
end

return firebase