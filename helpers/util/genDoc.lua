function client:generate(settings, dir)
    local data = ''

    function addHeading(str, sub)
        local Sub = ''
        for i = 1, sub do
            Sub = Sub .. '#'
        end

        data = data .. Sub .. ' ' .. str .. '\n'
    end

    function addText(str)
        data = data .. str .. '\n'
    end

    function addBreak()
        if not settings.includeBreaks then
            data = data .. '\n'
        else
            data = data .. '<br/>\n'
        end
    end

    function createTableHeader(...)
        local headings = {...}

        local heading = ''

        for _,v in pairs(headings) do
            heading = heading .. '|' .. v
        end

        heading = heading .. '|\n'
        
        for _,v in pairs(headings) do
            heading = heading.. '|' .. '---'
        end

        heading = heading .. '|'

        data = data .. heading .. '\n'
    end

    function addTableRow(...)
        local cols = {...}
        local row = ''

        for _,v in pairs(cols) do
            row = row .. '|' .. v
        end

        row = row .. '|'

        data = data .. row .. '\n'
    end

    function addImage(url)
        data = data .. '<img src="' .. url .. '">\n'
    end

    local username = self.dClient.user.name

    local cats = {}

    for _,v in pairs(categories) do
        table.insert(cats, v)
    end

    local multi = #cats > 1
    local extra = ''
    if multi then 
        multi = 'multi-purpose' 
        extra = ''
        for i,v in pairs(cats) do
            if i ~= 1 then
                extra = v.name:lower() .. ',' .. extra
            else
                extra = 'and ' .. v.name:lower() .. ',' .. extra
            end
        end

        extra = 'with features like ' .. extra

        extra = extra:sub(0,extra:len()-1) .. '.'
    else 
        multi = cats[1].name 
        extra = '.'
    end

    local details = 'This bot is'
    local part = 1

    function addDetails(thing)
        if part == 1 then
            details = details .. ' ' .. thing
        elseif part == 2 and part ~= #settings.details then
            details = details .. ', ' .. thing ..' '
        elseif part == 3 or 2 == #settings.details then
            details = details .. 'and ' .. thing ..'.'
        end

        part = part + 1
    end

    if settings['details']['openSourced'] then
        addDetails('open sourced')
    end
    if settings['details']['updatesRegularly'] then
        addDetails('updated regularly')
    end
    if settings['details']['suggestions'] then
        addDetails('open to suggestions')
    end

    local commandsText

    if #Commands < 50 then
        commandsText = 'We have '.. #Commands .. ' and more are being added.'
    else
        commandsText = 'We have over ' .. math.floor(#Commands/10)*10 .. ' commands.'
    end

    if settings['headerImage'] then
        addText('<p align="center">')
        addImage(settings.headerImage)
        addText('</p>')
        addText('')
    end

    if settings['badges'] then
        addText('<p align="center">')
        for _,v in pairs(settings.badges) do
            addImage(v)
        end
        addText('</p>')
        addText('')
    end

    local preface = settings['removeWatermark']

    addHeading(username, 1)

    if not preface then
        addText('*Generated programmatically*')
        addBreak()
    end
    addText(username .. ' is a ' .. multi .. ' bot ' .. extra .. details .. commandsText)
    addBreak()
    addText('The default prefix is ' .. self.prefix .. ' and to get a more updated list use ' .. self.prefix .. 'help')
    addText('In commands, &lt;optional&gt; and [required].')
    for i,v in pairs(categories) do
        addHeading(i, 2)
        addText(v.description)
        addBreak()
        createTableHeader('Name', 'Aliases', 'Description', 'Usage')
        for _,command in pairs(v.commands) do
            if not command.hidden then
                local alias = ''

                if not command.aliases or #command.aliases == 0 then
                    alias = 'None'
                else
                    if #command.aliases > 1 then
                        for _,v in pairs(command.aliases) do
                            alias = alias .. v .. ','
                        end
                    else
                        alias = command.aliases[1]
                    end
                end

                command.usage = command.usage:gsub('<', '&lt;')
                command.usage = command.usage:gsub('>', '&gt;')

                addTableRow(command.name, alias, command.description, command.name .. ' ' .. command.usage)
            end
        end
    end

    fs.writeFileSync(dir, data)
    print(string.format('%s | \27[1;92m[SUCCESS]\27[0m | %s', os.date('%F %T'), 'Generated Readme'))
end