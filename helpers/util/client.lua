local Discordia = require('discordia')
local fs = require('fs')

local class = require('./class')
local commands = require('./command')
local promise = require('../libs/promise')

local client = class('Client')

local Imports = {}
local Requires = {}

local commands = {}
local cogs = {}

local cachedCogs = {}

require('./extenstions')()

function run(msg,prefix, client)
    local Command = string.split(msg.content, ' ')

    Command = string.gsub(Command[1],prefix, '')

    local args = string.split(msg.content, ' ')
    args = table.slice(args, 2)

    Command = string.lower(Command)
    for _,v in pairs(commands) do
        if v then
            if v:run(Command,msg,args,client.dClient, client) then
                break
            end
        end
    end
end

function client:initialize(prefix, token, options)
    local dClient = Discordia.Client()

    if not options then options = {} end

    _G.client = self
    _G.Class = require('./class')

    if token:sub(0,3) == 'env' then
        local key = string.split(token, ';')[2]
        require('./dotenv').config()

        if process.env[key] then
            token = process.env[key]
        end
    end

    self.token = token
    self.prefix = prefix
    self.dClient = dClient
    self.discordia = Discordia
    self.cog = require('./cog')

    self.owner = options.owner or 0

    self.listeners = {}
    self.readyListeners = {}
    self.messageListers = {}

    self.cogs = cogs

    self.genDocs = options.genDocs or {}

    self.firebase = options.firebase or false

    self.loaded = false

    if options.firebase then
        local modelFiles = fs.readdirSync(options.firebase)
        local models = {}

        for _,v in pairs(modelFiles) do
            if string.endswith(v,'.lua') then
                local name = v:sub(1 , #v - 4)

                local model = require(options.firebase .. '/' .. name)

                table.insert(models,model)
            end
        end

        require('helpers/cogs/firebase')(models)

        -- These need firebase

        require('helpers/cogs/settings')
        require('helpers/cogs/roles')
        require('helpers/cogs/welcomer')
    else
        self.loaded = true
    end

    if options.loadDefaultCogs then
        local cogs = fs.readdirSync('helpers/cogs')

        local exclude = {
          firebase = true, -- It won't function if loaded like this
          settings = true, -- Settings needs firebase
          roles = true,
          welcomer = true
        }

        for _,v in pairs(cogs) do
            if string.endswith(v,'.lua') then
                local name = v:sub(1 , #v - 4)
                if not exclude[name] then
                    require('helpers/cogs/' .. name)
                end
            end
        end
    end

    self.dClient:on('ready', function()
        for _,v in pairs(self.readyListeners) do
            v(msg)
        end
        print(string.format('%s | \27[1;92m[SUCCESS]\27[0m | %s', os.date('%F %T'), 'Ready as ' .. self.dClient.user.name))
    end)

    self.dClient:on('messageCreate', function(msg)
        for _,v in pairs(self.messageListers) do
            v(msg)
        end

        local Promise = promise.new()

        local startsWith = false

        if msg.channel.type == 0 and self.loaded then
            if self.firebase then
                local firebase = self:getCog('Firebase')

                firebase:getGuild(msg.guild):next(function(guild)
                    local data = guild.data

                    if string.startswith(msg.content,data.prefix) then
                        local restricted = false

                        local Command = string.split(msg.content, ' ')

                        Command = string.gsub(Command[1],data.prefix, '')

                        if not data.restrictedChannels then
                            data.restrictedChannels = {}
                        end

                        for _,v in pairs(data.restrictedChannels) do
                            if v == msg.channel.id or v == Command:lower() then -- Restrict a channel or a command
                                restricted = true
                            end
                        end

                        if not restricted then
                            startsWith = data.prefix
                        end
                    end
                    Promise:resolve(startsWith)
                end)
            else
                if string.startswith(msg.content,self.prefix) then
                    startsWith = self.prefix
                end
                Promise:resolve(startsWith)
            end
        elseif self.loaded then
            if string.startswith(msg.content,self.prefix) then
                startsWith = self.prefix
            end
            Promise:resolve(startsWith)
        else
            Promise:resolve(false) -- The command won't run
        end

        Promise:next(function(startsWith)
            if startsWith then
                local succ, err = pcall(function()
                    coroutine.wrap(function()
                        run(msg,startsWith, self)
                    end)()
                end)

                if not succ then
                    print(string.format('%s | \27[1;31m[ERROR]\27[0m   | %s', os.date('%F %T'), 'Command Error | ' .. err))
                    msg.channel:send('Something went wrong, try again later')
                end
            end
        end)
    end)
end

function client:on(event, cb, name)
    if event == 'messageCreate' then
        table.insert(self.messageListers,cb)
    elseif event == 'ready' then
        table.insert(self.readyListeners,cb)
    else
        if not self.listeners[event] then self.listeners[event] = {} end

        self.listeners[event][name] = cb

        for i,v in pairs(self.listeners) do
            self.dClient:on(i, function(...)
                local vals = {...}
                for _,a in pairs(v) do
                    a(unpack(vals))
                end
            end)
        end
    end
end

function client:import(name,tbl)
    if tbl then
        Imports[name] = tbl
    elseif Imports[name] then
        return self:getImport(name)
    else
        local suc,err = pcall(function()
            require('../libs/' .. name)
        end)

        if suc then
            Imports[name] = require('../libs/' .. name)
            return Imports[name]
        else
            Imports[name] = require(name)
            return Imports[name]
        end
    end
end

function client:setUp(name)
    local module = Imports[name]

    Imports[name] = module()
end

function client:require(name,tbl)
    if not Requires[name] then
        Requires[name] = require(name)
    end
    return Requires[name]
end

function client:preLoad(tbl)
    for _,v in pairs(tbl) do
        Imports[v] = require(v)
    end
end

function client:getImport(name)
    return Imports[name]
end

function client:addCategory(name,description)
    categories[name] = {
        name = name,
        description = description,
        commands = {}
    }
end

function client:addCog(cog)
    local cogModule

    if type(cog) == 'string' then
        local suc,err = pcall( function() require('cogs/' .. cog) end)
        if err then
            suc,err = pcall( function() require('helpers/cogs/' .. cog) end)
            if err then
                if cachedCogs[cog] then
                    cogModule = cachedCogs[cog]
                else
                    error('Cog not found')
                end
            end
        end
    else
        cogModule = cog
    end

    cogs[cogModule.class.name] = cogModule

    for i,v in pairs(cogModule) do
        if type(v) == 'table' then
            if v['class'] then
                if v.class.name == 'Command' then
                    v.name = i
                    commands[i] = v
                end
            end
        end
    end
end

function client:removeCog(cog)
    local Cog = cogs[cog]

    if Cog then
        cachedCogs[cog] = Cog
        cogs[cog] = nil
    else
        return error(cog .. ' does\'t exist')
    end

    for i,v in pairs(Cog) do
        if type(v) == 'table' then
            if v['class'] then
                if v.class.name == 'Command' then
                    commands[i] = nil
                end
            end
        end
    end
end

function client:getCog(name)
    return cogs[name]
end

function client:run(status)
    self.dClient:run('Bot ' .. self.token)
    self.dClient:setGame(status)
end

return client
