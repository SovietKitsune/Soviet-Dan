local fs = require('fs')
local path = require('path')

function log (message)
  print('[dotenv][DEBUG] ' .. message)
end

local NEWLINE = '\n'

-- Parses src into an Object
function parse (src)
    local debug = true
    local obj = {}

    -- convert Buffers before splitting into lines and processing
    for idx,line in pairs(string.split(tostring(src),'\n')) do
        -- matching "KEY' and 'VAL' in 'KEY=VAL'
        local keyValueArr = string.split(line, "=")
        -- matched?
        if (#keyValueArr > 1) then
            local key = keyValueArr[1]
            -- default undefined or missing values to empty string
            local val = (keyValueArr[2] or '')
            local End = val:len()
            local isDoubleQuoted = val:sub(0,1) == '"' and val:sub(End,End) == '"'
            local isSingleQuoted = val:sub(0,1) == "'" and val:sub(End,End) == "'"

            -- if single or double quoted, remove quotes
            if (isSingleQuoted or isDoubleQuoted) then
                val = val:sub(1, End)

                -- if double quoted, expand newlines
                if (isDoubleQuoted) then
                val = val:gsub('\n', NEWLINE)
                end
            else
            -- remove surrounding whitespace
                val = val:match('^%s*(.-)%s*$')
            end

            obj[key] = val
        elseif (debug) then
            log('did not match key and value when parsing line ' .. idx .. ': ' .. line)
        end
    end 
  return obj
end

-- Populates process.env from .env file
function config (options)
    local dotenvPath = path.resolve(process.cwd(), '.env')
    local encoding = 'utf8'
    local debug = false

    if (options) then
        if (options.path ~= nil) then
            dotenvPath = options.path
        end
        if (options.encoding ~= nil) then
            encoding = options.encoding
        end
        if (options.debug ~= nil) then
            debug = true
        end
    end

    local Parsed

    local suc,err = pcall(function() 
        local parsed = parse(fs.readFileSync(dotenvPath, { encoding }), { debug })

        for i,v in pairs(parsed) do
            if not process.env[i] then
                process.env[i] = v
            else
                log(i .. ' is already already defined in `process.env` and will not be overwritten')
            end
        end

        Parsed = {parsed}
    end)
    -- specifying an encoding returns a string instead of a buffer
    if not suc then
        error(err)
    end
end

return {
    config = config,
    parse = parse
}
