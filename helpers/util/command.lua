--[=[
@c command
@t ui
@p name string
@p run function
@op data dictionary
@op require function
@d A constructor for commands
]=]

local class = require('./class')

local command = class('Command')

function command:initialize(func, data)
    self.func = func

    if not data then data = {} end

    self.aliases = data.aliases or {}
    self.description = data.description or 'None'
    self.usage = data.usage or nil

    self.ownerOnly = data.ownerOnly or false
    self.guildOnly = data.guildOnly or false
end

--[=[
@m check
@p command string
@r boolean
@d Checks if the command is this
]=]
function command:check(Command, msg,client, owner)
    local check = Command:lower() == self.name or table.search(self.aliases, Command:lower())

    if self.guildOnly and check then
        if not msg.guild then
            check = false
        end
    end

    if self.ownerOnly and check then
        if msg.author.id ~= owner.owner then
            check = false
        end
    end

    return check
end

--[=[
@m run
@p command string
@p message message
@p args table
@p client client
@r boolean
@d Runs the command
]=]
function command:run(Command, msg, args,client,owner)
    if self:check(Command,msg,client,owner) then
        self.func(msg,args,client)
        return true
    end
end

return command
