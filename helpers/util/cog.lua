local class = require('./class')

local cog = class('Cog')

-- Seems like a library

cog.command = require('./command')

function cog:initialize(client)
    self.client = client
end

function cog:on(event, cb)
    self.client:on(event,cb)
end

function cog:log(type,...)
    local data = {...}
    local level = ''
    local color = '32m'

    if type == 'info' then
        level = '[INFO]   '
    elseif type == 'success' then
        level = '[SUCCESS]'
    elseif type == 'warning' then
        level = '[WARNING]'
        color = '33m'
    elseif type == 'debug' then
        level = '[DEBUG]  '
        color = '33m'
    elseif type == 'error' then
        level = '[ERROR]  '
        color = '31m'
    end

    for _,v in pairs(data) do
        print(string.format('%s | \27[1;' .. color .. level ..'\27[0m | %s', os.date('%F %T'), v))
    end
end

return cog