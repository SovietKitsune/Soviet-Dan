local firebase = Class('Firebase', client.cog)

local Firebase = client:import('firebase')
local promise = client:import('promise')
local json = client:require('json')

function firebase:initialize(Client, schemas)
    client.cog.initialize(self, Client)

    local Promise = promise.new()

    local db = Firebase:new(process.env.DATABASE, process.env.FIREBASE, {
        email = process.env.EMAIL,
        password = process.env.PASSWORD
    },false, function(db)
        db:request('global','GET',function(err, data)
            if err then
                self:log('error',err.error)
            else
                if not data then
                    db:request('global','PUT', function(err)
                        if err then
                            self:log('error',err.error)
                        else
                            self.global = {
                                remainder = 10000000
                            }
                            Promise:resolve(db)
                        end
                    end, json.encode({
                        remainder = 10000000
                    }))
                else
                    self.global = data
                    Promise:resolve(db)
                end
            end
        end)
    end)

    self.cache = {}
    self.schemas = {}

    Promise:next(function(db)
        -- The database takes a little to make
        for _,v in pairs(schemas) do
            local Schema = db:newSchema(v.name,v.schema)
            self.schemas[v.name] = Schema
        end
        self:log('success','Firebase Ready')
        client.loaded = true
    end, function(err)
        self:log('error',err)
    end)
end

function firebase:getUser(author)
    local user = self.schemas.User

    if not self.cache.user then self.cache.user = {} end

    local Promise = promise.new()

    function handle(usr)
        usr:getData():next(function(user)
            Promise:resolve(user)
        end)
    end

    if self.cache.user[author.id] then
        handle(self.cache.user[author.id])
    else
        local id = author.id
        user:get(id):next(function(usr)
            if usr == 'None' then
                user:create({
                    id = id
                }):next(function(usr)
                    self.cache.user[id] = usr
                    handle(usr)
                end)
            else
                self.cache.user[id] = usr
                handle(usr)
            end
        end)
    end

    return Promise
end

function firebase:getGuild(server)
    local guild = self.schemas.Guild

    if not self.cache.guild then self.cache.guild = {} end

    local Promise = promise.new()

    function handle(gld)
        gld:getData():next(function(Guild)
            Promise:resolve(Guild)
        end)
    end

    if self.cache.guild[server.id] then
        handle(self.cache.guild[server.id])
    else
        local id = server.id
        guild:get(id):next(function(gld)
            if gld == 'None' then
                guild:create({
                    id = id
                }):next(function(gld)
                    self.cache.guild[id] = gld
                    handle(gld)
                end)
            else
                self.cache.guild[id] = gld
                handle(gld)
            end
        end)
    end

    return Promise
end

function firebase:getGlobal()
    return self.global
end

function firebase:saveGlobal()
    db:request('global','PUT',function(err, data)
        if err then
            self:log('error',err.error)
        end
    end, json.encode(self.global))
end

return function(schemas)
    client:addCog(firebase:new(client,schemas))
end

