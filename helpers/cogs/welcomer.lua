local welcomer = Class('Welcomer', client.cog)
local richEmbed = client:getImport('embed')

function welcomer:initialize(Client)
    client.cog.initialize(self, Client)

    client:on('ready', function()
        self.utils = client:getCog('Utils') -- The thing won't be ready until after this loads
        self.roles = client:getCog('Roles')
        self.firebase = client:getCog('Firebase')
    end)

    client.dClient:on('memberJoin', function(member)
        self.firebase:getGuild(member.guild):next(function(guild)
            local data = guild.data

            if not data.welcomer then data.welcomer = 0 end

            self.roles:join(member,data)

            if data.welcomer ~= 0 then
                local channel = member.guild.textChannels:get(data.welcomer)
                if channel then
                    local embed = richEmbed:new()
                    embed:setTitle('Welcome')
                    embed:setDescription('Welcome <@' .. member.id .. '> to ' .. member.guild.name .. '. Hope you enjoy your stay.')

                    coroutine.wrap(function()
                        channel:send(embed:getTable())
                    end)()
                end
            end
        end)
    end)

    client.dClient:on('memberLeave', function(member)
        self.firebase:getGuild(member.guild):next(function(guild)
            local data = guild.data

            if not data.leaver then data.leaver = 0 end

            if data.leaver ~= 0 then
                local channel = member.guild.textChannels:get(data.leaver)
                if channel then
                    local embed = richEmbed:new()
                    embed:setTitle('Leave')
                    embed:setDescription('Bye <@' .. member.id .. '> hope you enjoyed your stay.')

                    coroutine.wrap(function()
                        channel:send(embed:getTable())
                    end)()
                end
            end
        end)
    end)


    self.setwelcome = self.command:new(function(msg, args)
        if self.utils:checkPerms(msg.member, 'manageGuild') then
            if #args == 1 then
                local channel = msg.mentionedChannels:toArray()[1]
                if not channel then return msg.channel:send('That channel does\'t exist!') end
                self.firebase:getGuild(msg.guild):next(function(guild)
                    local data = guild.data
                    if not data.welcomer then data.welcomer = 0 end

                    data.welcomer = channel.id

                    guild()

                    coroutine.wrap(function()
                        msg.channel:send('The welcome channel has been changed')
                    end)()
                end)
            else
                msg.channel:send('You need to specify a channel!')
            end
        else
            msg.channel:send('You need `bot manager` role or `manageGuild` permission.')
        end
    end, {
        guildOnly = true,
        description = 'Set/update the current welcome channel',
        usage = '[channel]'
    })

    self.setleave = self.command:new(function(msg, args)
        if self.utils:checkPerms(msg.member, 'manageGuild') then
            if #args == 1 then
                local channel = msg.mentionedChannels:toArray()[1]
                if not channel then return msg.channel:send('That channel does\'t exist!') end
                self.firebase:getGuild(msg.guild):next(function(guild)
                    local data = guild.data
                    if not data.leaver then data.leaver = 0 end

                    data.leaver = channel.id

                    guild()

                    coroutine.wrap(function()
                        msg.channel:send('The leave channel has been changed')
                    end)()
                end)
            else
                msg.channel:send('You need to specify a channel!')
            end
        else
            msg.channel:send('You need `bot manager` role or `manageGuild` permission.')
        end
    end, {
        guildOnly = true,
        description = 'Set/update the current leave channel',
        usage = '[channel]'
    })
end

client:addCog(welcomer:new())
