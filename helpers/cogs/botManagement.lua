local management = Class('Management', client.cog)

local RichEmbed = client:import('embed')

function management:initialize(Client, schemas)
    client.cog.initialize(self, Client)

    local sandbox = client:import('sandbox')
    local promise = client:import('promise')
    local fs = client:require('fs')

    self.pp = client:require('pretty-print')

    self.unload = self.command:new(function(msg,args)
        local toUnload = args[1]

        local suc,err = pcall(function() client:removeCog(toUnload) end)

        local embed = RichEmbed:new()
        embed:setTitle('Bot Management')

        if not suc then
            embed:setDescription('Failed to unload cog\n\nError: ' .. err)
        else
            embed:setDescription('Unloaded cog successfully')
        end

        msg.channel:send(embed:getTable())
    end,{
        ownerOnly = true,
        description = 'Unload a cog from the bot',
        usage = '[cog name]'
    })

    self.load = self.command:new(function(msg,args)
        local toLoad = args[1]
        local suc,err = pcall(function() client:addCog(toLoad) end)

        local embed = RichEmbed:new()
        embed:setTitle('Bot Management')

        if not suc then
            embed:setDescription('Failed to load cog\n\nError: ' .. err)
        else
            embed:setDescription('Loaded cog successfully')
        end

        msg.channel:send(embed:getTable())
    end,{
        ownerOnly = true,
        description = 'Load a cog back up',
        usage = '[cog name]'
    })

    self.execute = self.command:new(function(msg,args)
        local toRun = table.concat(args,' ')

        self:executeCommand(msg,toRun)
    end,{
        ownerOnly = true,
        description = 'Execute a command',
        usage = '[command]'
    })

    self.eval = self.command:new(function(msg,args)
        local sandbox = client:import('sandbox')
        self:evaluate(args, msg, sandbox)
    end,{
        ownerOnly = true,
        description = 'Evaluate some lua',
        usage = '[code]'
    })

    self.quit = self.command:new(function(msg)
        local embed = RichEmbed:new()
        embed:setTitle('Bot Management')
        embed:setDescription('Goodnight')

        msg.channel:send(embed:getTable())
        client.dClient:stop()
    end,{
        ownerOnly = true,
        description = 'Quit the bot'
    })
end

function management:code(str)
    return string.format('```\n%s```', str)
end

function management:prettyLine(...)
    local ret = {}
    for i = 1, select('#', ...) do
        local arg = self.pp.strip(pp.dump(select(i, ...)))
        table.insert(ret, arg)
    end
    return table.concat(ret, '\t')
end

function management:printLine(...)
    local ret = {}
    for i = 1, select('#', ...) do
        local arg = tostring(select(i, ...))
        table.insert(ret, arg)
    end
    return table.concat(ret, '\t')
end

function management:evaluate(arg, msg, sandbox)
    if not arg then return end

    arg = table.concat(arg, ' ')

    arg = arg:gsub('```\n?', '') -- strip markdown codeblocks

    local lines = {}

    local embed = RichEmbed:new()
    embed:setTitle('Evaluation')

    local success, runtimeError = pcall(function()
        sandbox.run(arg, {
            env = {
              print = function(...)
                  table.insert(lines, self:printLine(...))
              end,
              p = function(...)
                  table.insert(lines, self:prettyLine(...))
              end,
              msg = msg
            },
            quota=10000
        })
    end)
    if not success then
        embed:setDescription('Output [error]\n' .. self:code(runtimeError))
        return msg:reply(embed:getTable())
    end

    lines = table.concat(lines, '\n')

    if #lines > 1990 then -- truncate long messages
        lines = lines:sub(1, 1970)
    end

    embed:setDescription('Output [success]\n' .. self:code(lines))
    return msg:reply(embed:getTable())
end

function management:executeCommand(msg,toRun)
    local embed = RichEmbed:new()
    embed:setTitle('Bot Management')
    embed:setDescription('Running, this might take a while\n**THIS WILL BLOCK THE THREAD**')
    local m = msg.channel:send(embed:getTable())

    coroutine.wrap(function()
        local handle = io.popen(toRun)
        local result = handle:read("*a")
        handle:close()

        if result:len() > 1970 then
            embed:setDescription('Output:\n`Description is too large to view`')
            m:setEmbed(embed:getRaw())
        else
            if result == '' then
                result = 'None'
            end
            embed:setDescription('Output:\n`' .. result .. '`')
            m:setEmbed(embed:getRaw())
        end
    end)()
end

client:addCog(management:new(client))
