local moderation = Class('Moderation', client.cog)
local discordia = client:require('discordia')

local defaults = {
    kickMessage = '%s has been kicked',
    banMessage = '%s has been banned',
    purgeMessage = '%s messages have been deleted',
    unbanMessage = '%s has been unbanned',
    botUnablekick = 'I can\'t seem to kick %s',
    botUnableban = 'I can\'t seem to ban %s',
    botUnablePurge = 'I can\'t seem to manage the messages',
    botUnableUnban = 'I can\'t seem to unban %s',
    userUnablekick = 'You don\'t have permissions to kick',
    userUnableban = 'You don\'t have permissions to ban',
    userUnablePurge = 'You don\'t have permissions to manage messages'
}

function moderation:initialize(Client)
    client.cog.initialize(self, Client)


    client:on('ready', function()
        self.utils = client:getCog('Utils')
    end)

    self.settings = defaults

    self.purge = self.command:new(function(msg,args)
        local me = msg.guild.me
        local messages = args[1]

        if not messages then return msg.channel:send('No messages specified') end

        if(not me:hasPermission(0x00002000)) then
            return msg.channel:send(self.settings.botUnablePurge)
        end
        if (not self.utils:checkPerms(msg.member, 'manageMessages')) then
            msg.channel:send(self.settings.userUnablePurge);
            return;
        end

        local suc,error = pcall(function()
            self:bulkDelete(msg,tonumber(messages))
        end)

        if not suc then
            msg.channel:send('Could\'t delete all the messages, try a lower number')
        else
            msg.channel:send('Successfully deleted ' .. messages .. ' messages') -- If no error it did them all
        end
    end, {
      guildOnly = true,
      description = 'Purge messages',
      usage = '[messages]'
    })

    self.ban = self.command:new(function(msg)
        local mentionedUser = msg.mentionedUsers.first
        self:removeUser(msg.guild.me,msg,mentionedUser,'ban')
    end, {
      guildOnly = true,
      description = 'Ban a member',
      usage = '[member mention]'
    })

    self.unban = self.command:new(function(msg,args)
        if self.utils:checkPerms(msg.member, 'banMembers') then
            local user = client.dClient:getUser(args[1])
            if not user then
                msg.channel:send('You need to specify a user')
            else
                if self.utils:checkPerms(msg.guild.me, 'banMembers') then
                else
                    if not me:hasPermission(0x00000004) then
                        return msg.channel:send(string.format(self.settings.botUnableUnban, '<@' .. user.id .. '>'))
                    else
                        local suc,err =  pcall(function() msg.guild.unban(user) end)
                        if not suc then
                            msg.channel:send(string.format(self.settings.botUnableUnban, '<@' .. user.id .. '>'))
                        else
                            msg.channel:send(string.format(self.settings.unbanMessage, '<@' .. user.id .. '>'))
                        end
                    end
                end
            end
        else
            msg.channel:send(self.settings.userUnableban)
        end
    end, {
      guildOnly = true,
      description = 'Unban a member',
      usage = '[member id]'
    })

    self.kick = self.command:new(function(msg)
        local mentionedUser = msg.mentionedUsers.first
        self:removeUser(msg.guild.me,msg,mentionedUser,'kick')
    end, {
      guildOnly = true,
      description = 'Kick a member',
      usage = '[member mention]'
    })
end

function moderation:removeUser(me, msg, member,type )
    if not member then
        return msg.channel:send('User Not Found!')
    end

    if not me:hasPermission(0x00000004) then
        return msg.channel:send(string.format(self.settings['botUnable' .. type], '<@' .. member.id .. '>'))
    end

    if not self.utils:checkPerms(msg.member, type .. 'Members') then
        return msg.channel:send(self.settings['userUnable' .. type])
    end

    local member = msg.guild:getMember(member.id)

    if self.utils:manageable(me, member) then
        if type == 'ban' then
            member:ban()
        else
            member:kick()
        end

        msg.channel:send(string.format(self.settings[type .. 'Message'], '<@' .. member.id .. '>'));
    else
        msg.channel:send(string.format(self.settings['botUnable' .. type], '<@' .. member.id .. '>'))
    end
end

function moderation:bulkDelete(msg, messages)
    if (type(messages) == 'table') then
        local messageIDs = {} --= messages.map(m => m.id || m);

        for _,m in pairs(messages) do
            table.insert(messageIDs, m.id or m)
        end

        if (#messageIDs == 0) then return {} end
        if (#messageIDs == 1) then
          msg.channel:getMessage(messageIDs[1]):delete()
          return {messageIDs[1]}
        end
        local channel = msg.guild:getChannel(msg.channel.id)

        channel:bulkDelete(messageIDs)

        return messageIDs
    elseif type(messages) == 'number' then
        local channel = msg.guild:getChannel(msg.channel.id)
        return self:bulkDelete(msg,channel:getMessages(messages), filterOld)
    end
end

client:addCog(moderation:new())
