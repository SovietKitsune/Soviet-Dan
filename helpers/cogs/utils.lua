local utils = Class('Utils', client.cog)
local discordia = client:require('discordia')
local enums = discordia.enums.permission

local roles = {
  administrator = {
    'administrator'
  },
  moderator = {
    'kickMembers',
    'banMembers',
    'manageMessages',
    'manageNicknames',
    'mentionEveryone'
  },
  ['bot manager'] = {
    'manageGuild',
    'manageRoles'
  },
  ['trail moderator'] = {
    'kickMembers',
    'manageMessages'
  }
}

function utils:initialize(Client)
    client.cog.initialize(self, Client)
end

function utils:escape(text)
    return text:gsub("([^%w])", "%%%1")
end

function utils:pingManager(msg,args,useSelf)
    local pings = {}

    if #msg.mentionedUsers:toArray() > 0 then
        return msg.mentionedUsers:toArray()
    else
        for _,v in pairs(args) do
            local user = msg.guild:getMember(v)
            if user then
                table.insert(pings,user)
            end
        end

        return pings
    end
end

function utils:compareRoles(role1,role2)
    if (role1.position == role2.position) then return role2.id - role1.id end
    return role1.position - role2.position
end

function utils:manageable(member)
    if (member.user.id == member.guild.ownerID) then return false end
    if (member.user.id == member.client.user.id) then return false end
    if (member.client.user.id == member.guild.ownerID) then return true end
    return self:compareRoles(member.guild.me.highestRole,member.highestRole) > 0;
end

function utils:manageable(mem1, mem2)
    -- mem1 is the one trying to manage mem2

    if self:checkPerms(mem2, 'kickMembers') then
        return false, 'That user is a mod/admin'
    else
        return self:manageable(mem1)
    end
end

function utils:checkPerms(user,permission)
    local perms = {}

    function hasRole(User,role)
        local roles = User.roles

        local has

        roles:forEach(function(Role)
            if Role.name:lower() == role:lower() then
                has = true
            end
        end)
        return has
    end

    if user:hasPermission(enums[permission]) then
        return true
    else
        if hasRole(user,'administrator') then
            return true
        else
            local roleNeeded = {}
            for a,v in pairs(roles) do
                for _,i in pairs(v) do
                    if i == permission then
                        table.insert(roleNeeded, a)
                    end
                end
            end
            local has
            if roleNeeded then
                for _,v in pairs(roleNeeded) do
                    if hasRole(user, v) then
                        return true
                    end
                end
            else
                return false
            end
        end
    end
end

client:addCog(utils:new())
