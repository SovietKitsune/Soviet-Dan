local settings = Class('Settings', client.cog)
local discordia = client:require('discordia')

function settings:initialize(Client)
    client.cog.initialize(self, Client)

    client:on('ready', function()
        self.utils = client:getCog('Utils') -- The thing won't be ready until after this loads
        self.firebase = client:getCog('Firebase')
    end)

    self.setprefix = self.command:new(function(msg,args)
        if self.utils:checkPerms(msg.member, 'manageGuild') then
            if #args == 1 then
                local prefix = args[1]
                if prefix:len() > 5 then
                    msg.channel:send('Prefix needs to be less then 5 characters')
                else
                    self.firebase:getGuild(msg.guild):next(function(guild)
                        local data = guild.data

                        print(self.utils:escape(prefix))

                        data.prefix = self.utils:escape(prefix)

                        guild()

                        coroutine.wrap(function()
                            msg.channel:send('Prefix has been changed to `' .. prefix .. '`')
                        end)()
                    end)
                end
            else
                msg.channel:send('You need to specify a prefix!')
            end
        else
            msg.channel:send('You `bot manager` role or `manageGuild` permission.')
        end
    end, {
      guildOnly = true,
      description = 'Change the guilds prefix',
      usage = '[prefix]'
    })

    self.restrict = self.command:new(function(msg,args)
        if self.utils:checkPerms(msg.member, 'manageGuild') then
            if #args == 1 then
                local toRestrict
                if #msg.mentionedChannels:toArray() == 1 then
                    toRestrict = msg.mentionedChannels:toArray()[1].id
                else
                    local Command
                    local cogs = client.cogs

                    for i,v in pairs(cogs) do
                        for name,command in pairs(v) do
                            if type(command) == 'table' then
                                if command['class'] then
                                    if command.class.name == 'Command' then
                                        if name == args[1]:lower() then
                                            Command = command
                                        end
                                    end
                                end
                            end
                        end
                    end

                    if not Command then
                        msg.channel:send(args[1] .. ' does\'t exist!')
                    else
                        toRestrict = Command:lower()
                    end
                end

                if toRestrict then
                    self.firebase:getGuild(msg.guild):next(function(guild)
                        local data = guild.data
                        if not data.restrictedChannels then data.restrictedChannels = {} end

                        local inAlready

                        for _,v in pairs(data.restrictedChannels) do
                            if v == toRestrict then
                                inAlready = false
                            end
                        end

                        if not inAlready then
                            table.insert(data.restrictedChannels, toRestrict)
                        end
                        guild()

                        coroutine.wrap(function()
                            msg.channel:send('The channel/command has been restricted')
                        end)()
                    end)
                end
            else
                msg.channel:send('You need to specify a command/channel!')
            end
        else
            msg.channel:send('You `bot manager` role or `manageGuild` permission.')
        end
    end, {
      guildOnly = true,
      description = 'Restrict a command or channel',
      usage = '[command/channel]'
    })

    self.unrestrict = self.command:new(function(msg,args)
        if self.utils:checkPerms(msg.member, 'manageGuild') then
            if #args == 1 then
                local toRestrict
                if #msg.mentionedChannels:toArray() == 1 then
                    toRestrict = msg.mentionedChannels:toArray()[1].id
                else
                    local Command
                    local cogs = client.cogs

                    for i,v in pairs(cogs) do
                        for name,command in pairs(v) do
                            if type(command) == 'table' then
                                if command['class'] then
                                    if command.class.name == 'Command' then
                                        if name == args[1]:lower() then
                                            Command = command
                                        end
                                    end
                                end
                            end
                        end
                    end

                    if not Command then
                        msg.channel:send(args[1] .. ' does\'t exist!')
                    else
                        toRestrict = Command:lower()
                    end
                end

                if toRestrict then
                    self.firebase:getGuild(msg.guild):next(function(guild)
                        local data = guild.data
                        if not data.restrictedChannels then data.restrictedChannels = {} end

                        local inAlready

                        for i,v in pairs(data.restrictedChannels) do
                            if v == toRestrict then
                                inAlready = i
                            end
                        end

                        if inAlready then
                            table.remove(data.restrictedChannels, inAlready)

                            if #data.restrictedChannels == 0 then
                                table.insert(data.restrictedChannels, 'To keep firebase happy')
                            end

                            guild()

                            coroutine.wrap(function()
                                msg.channel:send('The channel/command has been unrestricted.')
                            end)()
                        else
                            coroutine.wrap(function()
                                msg.channel:send('The channel/command was\'t restricted.')
                            end)()
                        end
                    end)
                end
            else
                msg.channel:send('You need to specify a command/channel!')
            end
        else
            msg.channel:send('You `bot manager` role or `manageGuild` permission.')
        end
    end, {
      guildOnly = true,
      description = 'Unrestrict a command or channel',
      usage = '[command/channel]'
    })
end

client:addCog(settings:new())
