local roles = Class('Roles', client.cog)
local richEmbed = client:getImport('embed')

function roles:initialize(Client)
    client.cog.initialize(self, Client)

    client:on('ready', function()
        self.utils = client:getCog('Utils') -- The thing won't be ready until after this loads
        self.firebase = client:getCog('Firebase')
    end)

    self.joinrole = self.command:new(function(msg, args)
        if self.utils:checkPerms(msg.member, 'manageRoles') then
            if #args == 1 then
                local role = msg.mentionedRoles:toArray()[1]
                if not role then
                    if msg.guild.roles:get(args[1]) then
                        role = msg.guild.roles:get(args[1])
                    else
                        return msg.channel:send('That role does not exist!')
                    end
                end
                if not msg.guild.me:hasPermission(0x10000000) then
                    return msg.channel:send('I don\'t have permissions to manage roles!')
                elseif self.utils:compareRoles(msg.guild.me.highestRole, role) < 0 then
                    return msg.channel:send('I can\'t manage that role!')
                end
                self.firebase:getGuild(msg.guild):next(function(guild)
                    local data = guild.data
                    if not data.joinRole then data.joinRole = 0 end

                    data.joinRole = role.id

                    guild()

                    coroutine.wrap(function()
                        msg.channel:send('The join role has been changed.')
                    end)()
                end)
            else
                msg.channel:send('You need to specify a role!')
            end
        else
            msg.channel:send('You need`bot manager` role or `manageRoles` permission.')
        end
    end, {
        guildOnly = true,
        description = 'Set/update the current new member role',
        usage = '[role]'
    })

    self.addrole = self.command:new(function(msg,args)
        if self.utils:checkPerms(msg.member, 'manageRoles') then
            if #args == 1 then
                local role = msg.mentionedRoles:toArray()[1]
                if not role then
                    if msg.guild.roles:get(args[1]) then
                        role = msg.guild.roles:get(args[1])
                    else
                        return msg.channel:send('That role does not exist!')
                    end
                end
                if not msg.guild.me:hasPermission(0x10000000) then
                    return msg.channel:send('I don\'t have permissions to manage roles!')
                elseif self.utils:compareRoles(msg.guild.me.highestRole, role) < 0 then
                    return msg.channel:send('I can\'t manage that role!')
                end
                self.firebase:getGuild(msg.guild):next(function(guild)
                    local data = guild.data
                    if not data.joinableRoles then data.joinableRoles = {} end

                    local inAlready

                    for _,v in pairs(data.joinableRoles) do
                        if v == role.id then
                            inAlready = false
                        end
                    end

                    if not inAlready then
                        table.insert(data.joinableRoles, role.id)
                    end

                    guild()

                    coroutine.wrap(function()
                        msg.channel:send('The role has been added.')
                    end)()
                end)
            else
                msg.channel:send('You need to specify a role!')
            end
        else
            msg.channel:send('You need`bot manager` role or `manageRoles` permission.')
        end
    end, {
        guildOnly = true,
        description = 'Add a role to member addable roles',
        usage = '[role]'
    })

    self.removerole = self.command:new(function(msg,args)
         if self.utils:checkPerms(msg.member, 'manageRoles') then
            if #args == 1 then
                local role = msg.mentionedRoles:toArray()[1]
                if not role then
                    if msg.guild.roles:get(args[1]) then
                        role = msg.guild.roles:get(args[1])
                    else
                        return msg.channel:send('That role does not exist!')
                    end
                end
                self.firebase:getGuild(msg.guild):next(function(guild)
                    local data = guild.data
                    if not data.joinableRoles then data.joinableRoles = {} end

                    local inRoles

                    for i,v in pairs(data.joinableRoles) do
                        if v == role.id then
                            inRoles = i
                            break
                        end
                    end

                    if inRoles then
                        table.remove(data.joinableRoles, inRoles)

                        if #data.joinableRoles == 0 then
                            table.insert(data.joinableRoles, 'firebase')
                        end

                        guild()

                        coroutine.wrap(function()
                            msg.channel:send('The role has been removed.')
                        end)()
                    else
                      coroutine.wrap(function()
                          msg.channel:send('The role was never joinable.')
                      end)()
                    end
                end)
            else
                msg.channel:send('You need to specify a role!')
            end
        else
            msg.channel:send('You need`bot manager` role or `manageRoles` permission.')
        end
    end, {
        guildOnly = true,
        description = 'Remove a role that members could join',
        usage = '[role]'
    })

    self.join = self.command:new(function(msg,args)
        if #args > 0 then
            local role = table.concat(args, ' ')
            self.firebase:getGuild(msg.guild):next(function(guild)
                local data = guild.data
                if not data.joinableRoles then data.joinableRoles = {} end

                local inRoles

                for _,v in pairs(data.joinableRoles) do
                    if v == role then
                        inRoles = v
                        break
                    else
                        local Role = msg.guild:getRole(v)
                        if Role then
                            if Role.name:lower() == role:lower() then
                                inRoles = Role.id
                                break
                            end
                        end
                    end
                end

                if not inRoles then
                    coroutine.wrap(function()
                        msg.channel:send('The role could not be found')
                    end)()
                else
                    coroutine.wrap(function()
                        msg.member:addRole(inRoles)
                        msg.channel:send('The role has been added')
                    end)()
                end
            end)
        else
            msg.channel:send('You need to specify a role!')
        end
    end)

    self.leave = self.command:new(function(msg,args)
        if #args > 0 then
            local role = table.concat(args, ' ')
            self.firebase:getGuild(msg.guild):next(function(guild)
                local data = guild.data
                if not data.joinableRoles then data.joinableRoles = {} end

                local inRoles

                for _,v in pairs(data.joinableRoles) do
                    if v == role then
                        inRoles = v
                        break
                    else
                        local Role = msg.guild:getRole(v)
                        if Role then
                            if Role.name:lower() == role:lower() then
                                inRoles = Role.id
                                break
                            end
                        end
                    end
                end

                if not inRoles then
                    coroutine.wrap(function()
                        msg.channel:send('The role could not be found')
                    end)()
                else
                    coroutine.wrap(function()
                        msg.member:removeRole(inRoles)
                        msg.channel:send('The role has been removed')
                    end)()
                end
            end)
        else
            msg.channel:send('You need to specify a role!')
        end
    end)

    self.roles = self.command:new(function(msg,args)
        self.firebase:getGuild(msg.guild):next(function(guild)
            local data = guild.data
            if not data.joinableRoles then data.joinableRoles = {} end

            if #data.joinableRoles == 0 or data.joinableRoles[1] == 'firebase' and #data.joinableRoles == 0 then
                msg.channel:send('There are no joinable roles')
            else
                local embed = richEmbed:new()
                :setTitle('Roles')

                local desc = ''

                for _,v in pairs(data.joinableRoles) do
                    if v ~= 'firebase' then
                        local Role = msg.guild:getRole(v)

                        desc = desc .. ', `'..  Role.name:lower() .. '`'
                    end
                end

                desc = desc:sub(2,desc:len())

                embed:setDescription(desc)

                coroutine.wrap(function()
                    msg.channel:send(embed:getTable())
                end)()
            end
        end)
    end)
end

function roles:join(member,data)
    if not data.joinRole then data.joinRole = 0 end

    if data.joinRole ~= 0 then
        local role = member.guild:getRole(data.joinRole)

        if role then
          coroutine.wrap(function()
              member:addRole(tostring(data.joinRole))
          end)()
        end
    end
end

client:addCog(roles:new())
