local information = Class('Information', client.cog)

function information:initialize(Client, schemas)
    client.cog.initialize(self, Client)

    local RichEmbed = client:import('embed')

    self.ping = self.command:new(function(msg)
        local m = msg.channel:send('Pinging...')

        local ping = m.createdAt-msg.createdAt
        m:setContent('Pong! Took ' .. tonumber(string.sub(ping, 0,4))*1000 .. 'ms')
    end, {
      description = 'Get the bots ping'
    })

    self.whois = self.command:new(function(msg)
        local user = msg.mentionedUsers.first
        if not user then
            user = msg.author
        end
        local member = msg.guild.members:get(user.id)

        local playing = 'None'
        local vc = 'None/Unknown'
        local limit = 'None/Unknown'
        local roles = ''

        if member.activity then
            playing = member.activity.name
        end

        if member.voiceChannel then
            limit = member.voiceChannel.userLimit
            vc = member.voiceChannel.name
        end

        member.roles:forEach(function(role)
            roles = roles .. '<@&' .. role.id .. '>'
        end)

        local embed = RichEmbed:new()
        :setTitle('Whois Information')
        :setAuthor(user.name, user.avatarURL)
        :addField(
            'Dates',
            'Joined Discord: ' .. user:getDate():toString() .. ' \n Joined Server: ' .. member.joinedAt,
            true
        )
        :addField(
            'Status',
            'Playing: ' .. member.activity.name .. '\nCurrent Status: ' ..  member.status,
            true
        )
        :addField(
            'General',
            'Tag: ' .. member.user.tag .. '\n Id: ' .. member.user.id,
            true
        )
        :addField(
            'Connections',
            'Voice Channel: ' .. vc ..'\n User Limit: ' .. limit,
            true
        )
        :addField(
            'Roles[' .. #member.roles:toArray() .. ']',
            roles,
            true
        )

        msg.channel:send(embed:getTable())
    end, {
        guildOnly = true,
        usage = '<user mention/id>',
        description = 'Get information on somebody else or yourself'
    })

    self.guildinfo = self.command:new(function(msg)
        local guild = msg.guild

        ---[[
        local embed = RichEmbed:new()
        :setTitle('Guild Information')
        :setAuthor(guild.name, guild.iconURL)
        :addField(
            'Dates',
            'Created: ' .. guild:getDate():toString(),
            true
        )
        :addField(
            'Owner: ',
            '<@' .. guild.owner.user.id .. '>',
            true
        )
        :addField(
            'Boosters',
            guild.premiumSubscriptionCount,
            true
        )
        :addField(
            'Region',
            guild.region,
            true
        )
        :addField(
            'Roles',
            #guild.roles:toArray(),
            true
        )
        :addField(
            'Stats',
            'Text channels: ' .. #guild.textChannels:toArray() ..
            '\nVoice channels: ' .. #guild.voiceChannels:toArray() ..
            '\nCategories: ' .. #guild.categories:toArray() ..
            '\nMembers: ' .. guild.totalMemberCount,
            true
        )
        --]]
        msg.channel:send(embed:getTable())
    end, {
        guildOnly = true,
        description = 'Get some guild information'
    })

    self.help = self.command:new(function(msg,args)
        local embed = RichEmbed:new()

        embed:setTitle('Help')

        if #args > 0 then
            args = table.concat(args, ' ')

            local Command
            local cogs = client.cogs

            for i,v in pairs(cogs) do
                for name,command in pairs(v) do
                    if type(command) == 'table' then
                        if command['class'] then
                            if command.class.name == 'Command' then
                                if name == args then
                                    Command = command
                                end
                            end
                        end
                    end
                end
            end

            if not Command then
                embed:setDescription('Command not found')
            elseif not command.ownerOnly or msg.author.id == client.owner then
                local aliases = ''

                for _,v in pairs(Command.aliases) do
                    aliases = aliases .. ', `' .. v .. '`'
                end

                if aliases ~= '' then
                    aliases = aliases:sub(2,aliases:len())
                else
                    aliases = 'None'
                end

                local usage = Command.usage or Command.name

                embed:setDescription(
                    'Name: ' .. Command.name ..
                    '\nDescription: ' .. Command.description ..
                    '\nAliases: ' .. aliases ..
                    '\nUsage: ' .. usage
                )
            end
        else
            local cogs = client.cogs

            for i,v in pairs(cogs) do

                local commands = ''

                for name,command in pairs(v) do
                    if type(command) == 'table' then
                        if command['class'] then
                            if command.class.name == 'Command' then
                                if not command.hidden then
                                    if not command.ownerOnly or msg.author.id == client.owner then
                                        commands = commands .. ',`' .. name .. '`'
                                    end
                                end
                            end
                        end
                    end
                end

                if commands ~= '' then
                    commands = commands:sub(2,commands:len())

                    embed:addField(i .. ' cog', commands)
                end
            end
        end

        msg.channel:send(embed:getTable())
    end, {
      description = 'The help command, to help you out',
      usage = '<Command Name>'
    })
end

client:addCog(information:new(client))


